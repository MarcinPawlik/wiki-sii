<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiControllerTest extends WebTestCase
{

    /**
     * Search title
     */
    public function testSearch()
    {
        $client = static::createClient();

        $client->request('POST', '/api/search', ['title' => 'Gdańsk']);

        $response = $client->getResponse();
        $data     = json_decode($response->getContent(), true);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals("Gdańsk", $data[0]['name']);
    }

    /**
     * Try search empty title
     */
    public function testSearchEmpty()
    {
        $client = static::createClient();

        $client->request('POST', '/api/search', ['title' => '']);

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    /**
     * Get article
     */
    public function testGet()
    {
        $client = static::createClient();

        $client->request('POST', '/api/get', ['title' => 'Gdańsk']);

        $response = $client->getResponse();
        $data     = json_decode($response->getContent(), true);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals("Gdańsk", $data['title']);
    }

    /**
     * Try get empty article
     */
    public function testGetEmpty()
    {
        $client = static::createClient();

        $client->request('POST', '/api/get', ['title' => '']);

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    /**
     * Try get dont exists article
     */
    public function testGetDontExists()
    {
        $client = static::createClient();

        $client->request('POST', '/api/get', ['title' => 'ArticleDontExists123']);

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }
}