<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DefaultController extends Controller
{

    /**
     * Homepage
     *
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        $form = $this->createFormBuilder()
            ->add('title', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Go'))
            ->getForm();

        return $this->render('AppBundle:default:index.html.twig', array(
                'form' => $form->createView(),
        ));
    }
}