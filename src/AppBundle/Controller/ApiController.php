<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\ApiService;

class ApiController extends Controller
{

    /**
     * Search article
     *
     * @Route("/api/search", name="api_search")
     * @Method("POST")
     */
    public function searchAction(Request $request)
    {
        $title = urlencode($request->get('title'));
        return $this->getApiService()->search($title);
    }

    /**
     * Get article
     *
     * @Route("/api/get", name="api_get")
     * @Method("POST")
     */
    public function getAction(Request $request)
    {
        $title = urlencode($request->get('title'));
        return $this->getApiService()->get($title);
    }

    /**
     * @DI\LookupMethod("api_service")
     * @return ApiService
     */
    public function getApiService()
    {

    }
}