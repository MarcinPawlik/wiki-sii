$(document).ready(function () {

    $("#form_title").autocomplete({
        source: function (request, response) {
            $.post(baseUrl + "api/search", {title: request.term}, function (data) {
                response($.map(data, function (item) {
                    return item.name;
                }));
            }).fail(function () {
                console.log('error');
            });
        },
        minLength: 2
    });

    $("form[name=form]").submit(function (e) {
        getData();
        e.preventDefault();
    });

    var getData = function () {
        $.post(baseUrl + "api/get", {title: $("#form_title").val()}, function (data) {
            renderArticle(data);
        }).fail(function () {
            noArticle();
            console.log('error');
        });
    }

    var renderArticle = function (data) {
        $("#articleTitle").html(data.title);
        $("#article").html(data.text['*']);
    }

    var noArticle = function (data) {
        $("#articleTitle").html('Article don\'\t exists');
        $("#article").html('');
    }
});