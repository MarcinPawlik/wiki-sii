<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Article;
use AppBundle\Repository\ArticleRepository;
use Buzz\Browser;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Validator\RecursiveValidator;

class ApiService
{
    /**
     * @var ArticleRepository
     */
    private $article;

    /**
     * @var Buzz
     */
    private $browser;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Validator
     */
    private $validator;

    /**
     *
     * @param ArticleRepository $article
     * @param Browser $browser
     * @param EntityManager $entityManager
     * @param Validator $validator
     */
    public function __construct(ArticleRepository $article, Browser $browser, EntityManager $entityManager, RecursiveValidator $validator)
    {
        $this->article   = $article;
        $this->browser   = $browser;
        $this->em        = $entityManager;
        $this->validator = $validator;
    }

    /**
     * Serach article by title
     *
     * @param type $title
     * @return JsonResponse
     */
    public function search($title)
    {
        if ($title == null) {
            return new JsonResponse([], 404);
        }
        $response = $this->browser->get('https://en.wikipedia.org/w/api.php?action=query&list=search&srsearch='.$title.'&format=json');
        if ($response->getStatusCode() == 200) {
            $data  = json_decode($response->getContent());
            $list  = $data->query->search;
            $array = [];
            foreach ($list as $el) {
                $array[] = ['name' => $el->title];
            }
            return new JsonResponse($array, 200);
        }
        return new JsonResponse([], 404);
    }

    /**
     * Get article by title
     *
     * @param type $title
     * @return JsonResponse
     */
    public function get($title)
    {
        $response = $this->browser->get('https://en.wikipedia.org/w/api.php?action=parse&section=0&prop=text&page='.$title.'&format=json');
        if ($response->getStatusCode() == 200) {
            $data = json_decode($response->getContent());
            if (isset($data->error)) {
                return new JsonResponse($data->error, 404);
            }
            $article = $data->parse;
            $this->save($article);
            return new JsonResponse($article, 200);
        }
        return new JsonResponse([], 404);
    }

    /**
     * Try save article to database
     *
     * @param type $object
     */
    private function save($object)
    {
        $title = $object->title;
        if ($this->article->exists($title)) {
            $article = new Article();
            $article->setTitle($title);
            $article->setText(serialize($object));

            $errors = $this->validator->validate($article);

            if (sizeof($errors) == 0) {
                $this->em->persist($article);
                $this->em->flush();
            }
        }
    }
}