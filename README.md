siiProjekt
==========

Controller:
* ApiController
* DefaultController

Entity:
* Article

Repository:
* ArticleRepository

Service:
* ApiService

UnitTest:
* ApiControllerTest
* DefaultControllerTest

Install
composer install
php app/console doctrine:migrations:migrate 

